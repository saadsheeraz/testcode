import React from "react";
import "./style.css";
import {
  Form,
  Button,
  Drawer,
  Spin,
  Space,
  Select,
  Popconfirm,
  message,
  Divider,
  TimePicker,
  DatePicker,
  Input,
  Row,
  Col,
  Table,
  Upload,
  Modal,
} from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  PlusOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import { storage } from "../firebase";
const { Option } = Select;
const { RangePicker } = TimePicker;
const key = "updatable";

export default class MedTable extends React.Component {
  state = {
    rows: ["default row"],
    searchField: "",
    modal2Visible: false,
    visible: false,
    _id: "",
    coloumns: [
      {
        title: "First Name",
        dataIndex: "firstName",
        key: "firstName",
      },
      {
        title: "Last Name",
        dataIndex: "lastName",
        key: "lastName",
      },
      {
        title: "Gender",
        dataIndex: "gender",
        filters: [
          { text: "Male", value: "Male" },
          { text: "Female", value: "Female" },
        ],
        onFilter: (value, item) =>
          item.gender ? item.gender.indexOf(value) === 0 : null,
        key: "gender",
      },
      {
        title: "Date of Birth",
        dataIndex: "dateofBirth",
        key: "dateofBirth",
      },
      {
        title: "Profession",
        dataIndex: "profession",
        key: "profession",
        filters: [
          { text: "commedtian", value: "commedtian" },
          { text: "actor", value: "actor" },
          { text: "actress", value: "actress" },
          { text: "model", value: "model" },
        ],
        onFilter: (value, item) =>
          item.profession ? item.profession.indexOf(value) === 0 : null,
      },
      {
        title: "Shoe Size",
        dataIndex: "shoeSize",
        key: "Shoe Size",
      },
      {
        title: "Hair Color",
        dataIndex: "hairColor",
        key: "hairColor",
        filters: [
          { text: "black", value: "black" },
          { text: "blonde", value: "blonde" },
          { text: "gray", value: "gray" },
        ],
        onFilter: (value, item) =>
          item.hairColor ? item.hairColor.indexOf(value) === 0 : null,
      },
      {
        title: "Hair Length",
        dataIndex: "hairLength",
        key: "hairLength",
        filters: [
          { text: "8-inches", value: "8-inches" },
          { text: "12-inches", value: "12-inches" },
          { text: "18-inches", value: "18-inches" },
          { text: "24-inches", value: "24-inches" },
        ],
        onFilter: (value, item) =>
          item.hairLength ? item.hairLength.indexOf(value) === 0 : null,
      },
      {
        title: "Bra Size",
        dataIndex: "braSize",
        key: "braSize",
      },
      {
        title: "Height",
        dataIndex: "height",
        key: "height",
        filters: [
          { text: "121-152", value: "121-152" },
          { text: "152-182", value: "152-182" },
          { text: "182-213", value: "182-213" },
        ],
        onFilter: (value, item) =>
          item.height ? item.height.indexOf(value) === 0 : null,
      },
      {
        title: "Weight",
        dataIndex: "weight",
        key: "weight",
      },
      {
        title: "Casting Type",
        dataIndex: "castingType",
        key: "castingType",
        filters: [
          { text: "movies", value: "movies" },
          { text: "commercials", value: "commercials" },
          { text: "newspapers", value: "newspapers" },
          { text: "magazines", value: "magazines" },
        ],
        onFilter: (value, item) =>
          item.castingType ? item.castingType.indexOf(value) === 0 : null,
      },

      {
        title: "Action",
        fixed: "right",
        width: "100",
        render: (text, item) => (
          <div>
            <EyeOutlined
              className="navBarText"
              alt="View"
              title="View"
              style={{ fontSize: "20px" }}
              onClick={() => {
                this.setState({
                  viewMode: true,
                  visible: true,
                  firstName: item.firstName,
                  lastName: item.lastName,
                  gender: item.gender,
                  dateofBirth: item.dateofBirth,
                  profession: item.profession,
                  shoeSize: item.shoeSize,
                  hairColor: item.hairColor,
                  hairLength: item.hairLength,
                  braSize: item.braSize,
                  waistSize: item.waistSize,
                  height: item.height,
                  weight: item.weight,
                  castingType: item.castingType,
                  filetoDisplay: item.picture,
                });
              }}
            />
          </div>
        ),
      },
    ],

    //---------------new
    firstName: "",
    lastName: "",
    gender: "",
    dateofBirth: "",
    profession: "",
    shoeSize: "",
    hairColor: "",
    hairLength: "",
    braSize: "",
    waistSize: "",
    height: "",
    weight: "",
    castingType: "",
    dataSource: [],
    //=======conditions
    viewMode: false,
    //=======for images
    previewVisible: false,
    previewImage: "",
    previewTitle: "",
    fileList: [],
    picturesForMongo: [],
    filetoDisplay: [],
  };

  getBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  }

  componentDidMount = () => {
    this.setState({ loading: true });
    // https://pacific-spire-14053.herokuapp.com/testFolder/testFile/all
    // fetch("http://localhost:5000/testFolder/testFile/all")
    //smiley-test-server.herokuapp.com/
    https: fetch(
      "https://smiley-test-server.herokuapp.com/testFolder/testFile/all"
    )
      .then((response) => response.json())
      .then((response) => {
        if (response) {
          console.log("response", response);
          this.setState({ dataSource: response, loading: false });
        } else {
          console.log("error");
        }
      });
  };
  handlePreview = async (file) => {
    console.log("handlePreview", file);
    if (!file.url && !file.preview) {
      file.preview = await this.getBase64(file.originFileObj);
    }

    this.setState({
      previewImage: file.url || file.preview,
      previewVisible: true,
      previewTitle:
        file.name || file.url.substring(file.url.lastIndexOf("/") + 1),
    });
  };
  handleCancel = () => this.setState({ previewVisible: false });

  handleChange = ({ fileList }) => {
    console.log("handleChange", fileList);
    this.setState({ fileList });
  };

  async handelUpload() {
    console.log("gettig hit", this.state.fileList);
    let stateFileList = this.state.fileList;
    let picturesLength = [];
    var pictureObj = {};
    stateFileList.map((item, index) => {
      const uploadScreenshot = storage
        .ref(`internetSpeedScreenshots/${item.originFileObj.name}`)
        .put(item.originFileObj);
      console.log("check 2");
      uploadScreenshot.on(
        "state_changes",
        (snapshot) => {},
        (error) => {
          console.log(error);
        },
        () => {
          storage
            .ref("internetSpeedScreenshots/")
            .child(item.originFileObj.name)
            .getDownloadURL()
            .then((url) => {
              pictureObj = {
                uid: index,
                name: item.originFileObj.name,
                status: item.status,
                url: url,
              };
              picturesLength.push(pictureObj);
              console.log("picturesURL", picturesLength);
              this.setState({ picturesForMongo: picturesLength });
            });
        }
      );
    });
  }

  onSubmit = () => {
    console.log("upload working");
    message.loading({
      content: "Loading!",
      key,
      duration: 7,
    });
    this.handelUpload();
    setTimeout(() => {
      console.log("pics", this.state.picturesForMongo);
      https://smiley-test-server.herokuapp.com/
      fetch("https://smiley-test-server.herokuapp.com/testFolder/testFile/postData", {
        method: "post",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          //Job Details
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          gender: this.state.gender,
          // picture: this.state.fileList,
          dateofBirth: this.state.dateofBirth,
          profession: this.state.profession,
          shoeSize: this.state.shoeSize,
          hairColor: this.state.hairColor,
          hairLength: this.state.hairLength,
          braSize: this.state.braSize,
          waistSize: this.state.waistSize,
          height: this.state.height,
          weight: this.state.weight,
          castingType: this.state.castingType,
          picture: this.state.picturesForMongo,
        }),
      })
        .then((response) => response.json())
        .then((response) => {
          console.log("submit response ", response);
          if (!response) {
            message.warning({
              content: "Error Adding Data!",
              key,
              duration: 2,
            });
          } else {
            message.success({
              content: "Successfully Added!",
              key,
              duration: 2,
            });
            this.onClose();
            this.componentDidMount();
          }
        });
    }, 7000);
  };

  handleSearchChange = (event) => {
    this.setState({ searchField: event.target.value });
  };

  showDrawer = () => {
    this.setState({
      visible: true,
      firstName: "",
      lastName: "",
      gender: "",
      dateofBirth: "",
      profession: "",
      shoeSize: "",
      hairColor: "",
      hairLength: "",
      braSize: "",
      waistSize: "",
      height: "",
      weight: "",
      castingType: "",

      //=======conditions
      viewMode: false,
      //=======for images
      previewVisible: false,
      previewImage: "",
      previewTitle: "",
      fileList: [],
      picturesForMongo: [],
    });
  };
  onClose = () => {
    console.log("close ");

    this.setState(
      {
        visible: false,
        edit: false,
        viewMode: false,
      },
      () => {
        console.log(this.state);
      }
    );
  };

  onchangeFname = (e) => {
    this.setState({ firstName: e.target.value });
  };
  onchangeLname = (e) => {
    this.setState({ lastName: e.target.value });
  };
  handleChangegender = (value) => {
    this.setState({ gender: value });
  };
  onChange_dob = (date, dateString) => {
    if (date && dateString) {
      console.log("date is", dateString);
      this.setState({ dateofBirth: dateString });
    }
  };
  handleChangeprofession = (value) => {
    this.setState({ profession: value });
  };
  handleChangeShoeSize = (value) => {
    this.setState({ shoeSize: value });
  };
  handleChangeHairColor = (value) => {
    this.setState({ hairColor: value });
  };
  handleChangeHairLength = (value) => {
    this.setState({ hairLength: value });
  };
  handleChangeBraSize = (value) => {
    this.setState({ braSize: value });
  };
  handleChangeWaist = (value) => {
    this.setState({ waistSize: value });
  };
  handleChangeHeight = (value) => {
    this.setState({ height: value });
  };
  handleChangeWeight = (value) => {
    this.setState({ weight: value });
  };
  handleChangeCasting = (value) => {
    this.setState({ castingType: value });
  };

  render() {
    const { edit, viewMode, searchField } = this.state;
    const { previewVisible, previewImage, fileList, previewTitle } = this.state;
    const uploadButton = (
      <div>
        <PlusOutlined />
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    );
    const filteredData = this.state.dataSource.filter((dep) => {
      return dep.firstName
        .toLowerCase()
        .includes(this.state.searchField.toLowerCase());
    });

    return (
      <div className="applicantPage">
        <Divider>Total Records</Divider>
        {this.state.loading === true ? (
          <div>
            <div className="loadingText"></div>
            <div className="tc f19 lh-copy">
              <Spin />
            </div>
          </div>
        ) : (
          <div>
            <Row style={{ marginRight: "20px" }}>
              <Col md="8">
                <Button
                  className="applyButtonProgram"
                  onClick={this.showDrawer}
                  style={{
                    backgroundColor: "green",
                    color: "white",
                  }}
                >
                  {" "}
                  Add new Record
                </Button>
              </Col>
              <Col md="4">
                {" "}
                <Input
                  placeholder="Search by Name.."
                  onChange={this.handleSearchChange}
                  type="search"
                  value={this.state.searchField}
                />
              </Col>
            </Row>

            <div className="applicantTable">
              <Table
                columns={this.state.coloumns}
                dataSource={filteredData}
                pagination={false}
                scroll={{
                  x: 1200,
                  y:
                    window.screen.height > 1000
                      ? window.screen.height - 475
                      : window.screen.height - 440,
                }}
              />
            </div>
          </div>
        )}

        <Drawer
          title={viewMode ? "View Record" : "Add Record"}
          width={window.innerWidth > 500 ? 1100 : window.innerWidth - 150}
          onClose={this.onClose}
          visible={this.state.visible}
          footer={
            <div
              style={{
                textAlign: "right",
              }}
            >
              {!viewMode ? (
                <Button
                  className="mButton"
                  style={{ marginRight: 8 }}
                  // disabled={this.state.submitcheck}
                  onClick={() => {
                    this.onSubmit();
                  }}
                  type="primary"
                >
                  Submit
                </Button>
              ) : null}
              <Button className="mButton" onClick={this.onClose}>
                Cancel
              </Button>
            </div>
          }
        >
          <Form id="create-user-form" layout="vertical" hideRequiredMark>
            <Divider>Record Details</Divider>
            <Row gutter={[16, 16]}>
              {/* firstname and lastname */}
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="First Name">
                    <h3>{this.state.firstName}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="First Name">
                    <Input
                      name="firstName"
                      value={this.state.firstName}
                      onChange={this.onchangeFname}
                      placeholder="Enter First Name"
                    />
                  </Form.Item>
                )}
              </Col>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Last Name">
                    <h3>{this.state.lastName}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Last Name">
                    <Input
                      name="lastname"
                      value={this.state.lastName}
                      onChange={this.onchangeLname}
                      placeholder="Enter First Name"
                    />
                  </Form.Item>
                )}
              </Col>
            </Row>
            {/* gender and dob */}
            <Row gutter={[16, 16]}>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label=" Gender">
                    <h3>{this.state.gender}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Select Gender">
                    <Select
                      // defaultValue="lucy"
                      style={{ width: "100%" }}
                      onChange={this.handleChangegender}
                      value={this.state.gender}
                    >
                      <Option value="Male">Male</Option>
                      <Option value="Female">Female</Option>
                      <Option value="notselected">Dont wish to answer</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Date Of Birth">
                    <h3>{this.state.dateofBirth}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Date Of Birth">
                    <Space direction="vertical">
                      <DatePicker onChange={this.onChange_dob} />
                    </Space>
                  </Form.Item>
                )}
              </Col>
            </Row>
            {/* profession and shoeSize */}
            <Row gutter={[16, 16]}>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Profession">
                    <h3>{this.state.profession}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Profession">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeprofession}
                      value={this.state.profession}
                    >
                      <Option value="commedtian">Commedtian</Option>
                      <Option value="actor">Actor</Option>
                      <Option value="actress">Actress</Option>
                      <Option value="model">model</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
              <Col span={12}>
                <Form.Item label="Shoe Size">
                  {viewMode ? (
                    <h3>{this.state.shoeSize}</h3>
                  ) : (
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeShoeSize}
                      value={this.state.shoeSize}
                    >
                      <Option value="No-6">No-6</Option>
                      <Option value="No-7">No-7</Option>
                      <Option value="No-8">No-8</Option>
                      <Option value="No-9">No-9</Option>
                      <Option value="No-10">No-10</Option>
                      <Option value="No-11">No-11</Option>
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            {/* haircolor and hairlength */}
            <Row gutter={[16, 16]}>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Hair Color">
                    {" "}
                    <h3>{this.state.hairColor}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Hair Color">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeHairColor}
                      value={this.state.hairColor}
                    >
                      <Option value="black">Black</Option>
                      <Option value="blonde">Blonde</Option>
                      <Option value="gray">Gray</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Hair Length">
                    <h3>{this.state.hairLength}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Hair Length">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeHairLength}
                      value={this.state.hairLength}
                    >
                      <Option value="8-inches">8-inches</Option>
                      <Option value="12-inches">12-inches</Option>
                      <Option value="18-inches">18-inches</Option>
                      <Option value="24-inches">24-inches</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
            </Row>
            {/* bra size  and waist size */}
            <Row gutter={[16, 16]}>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Bra Size">
                    <h3>{this.state.braSize}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Bra Size">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeBraSize}
                      value={this.state.braSize}
                    >
                      <Option value="28">28</Option>
                      <Option value="30">30</Option>
                      <Option value="32">32</Option>
                      <Option value="34">34</Option>
                      <Option value="36">36</Option>
                      <Option value="40">40</Option>
                      <Option value="42">42</Option>
                      <Option value="44">44</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Waist Size">
                    <h3>{this.state.waistSize}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Waist Size">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeWaist}
                      value={this.state.waistSize}
                    >
                      <Option value="20-30">20 to 30</Option>
                      <Option value="40-59">40 to 59</Option>
                      <Option value="60-over">60 and over</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
            </Row>

            {/* height and weight */}
            <Row gutter={[16, 16]}>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Height">
                    <h3>{this.state.height}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Height">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeHeight}
                      value={this.state.height}
                    >
                      <Option value="121-152">121-152(cm)</Option>
                      <Option value="152-182">152-182(cm)</Option>
                      <Option value="182-213">182-213(cm)</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Weight">
                    <h3>{this.state.weight}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Weight">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeWeight}
                      value={this.state.weight}
                    >
                      <Option value="30-40">30-40(Kg)</Option>
                      <Option value="41-50">41-50(Kg)</Option>
                      <Option value="51-60">51-60(Kg)</Option>
                      <Option value="61-70">61-70(Kg)</Option>
                      <Option value="71-80">71-80(Kg)</Option>
                      <Option value="81-90">81-90(Kg)</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
            </Row>

            {/* type of casting */}
            <Row gutter={[16, 16]}>
              <Col span={12}>
                {viewMode ? (
                  <Form.Item label="Type of Casting">
                    <h3>{this.state.castingType}</h3>
                  </Form.Item>
                ) : (
                  <Form.Item label="Type of Casting">
                    <Select
                      style={{ width: "100%" }}
                      onChange={this.handleChangeCasting}
                      value={this.state.castingType}
                    >
                      <Option value="movies">Movies</Option>
                      <Option value="commercials">Commercials</Option>
                      <Option value="newspapers">Newspapers</Option>
                      <Option value="magazines">Magazines</Option>
                    </Select>
                  </Form.Item>
                )}
              </Col>
              <Col span={12}>
                {viewMode ? (
                  <div>
                    <Modal
                      visible={previewVisible}
                      title={previewTitle}
                      footer={null}
                      onCancel={this.handleCancel}
                    >
                      <img
                        alt="example"
                        style={{ width: "100%" }}
                        src={previewImage}
                      />
                    </Modal>
                    <Upload
                      action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                      listType="picture-card"
                      fileList={this.state.filetoDisplay}
                      // onChange={onChange}
                      onPreview={this.handlePreview}
                    >
                      {/* {fileList.length < 5 && "+ Upload"} */}
                    </Upload>
                  </div>
                ) : (
                  <div>
                    <Modal
                      visible={viewMode}
                      title={previewTitle}
                      footer={null}
                      onCancel={this.handleCancel}
                    >
                      <img
                        alt="example"
                        style={{ width: "100%" }}
                        src={previewImage}
                      />
                    </Modal>
                    <Upload
                      action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                      listType="picture-card"
                      fileList={fileList}
                      onPreview={this.handlePreview}
                      onChange={this.handleChange}
                    >
                      {fileList.length >= 8 ? null : uploadButton}
                    </Upload>
                  </div>
                )}
              </Col>
            </Row>
          </Form>

          <Row gutter={16}>
            <Col span={4}>
              {edit ? (
                <Popconfirm
                  placement="right"
                  title="Do you want to delete ？"
                  okText="Yes"
                  cancelText="No"
                  onConfirm={() => {
                    this.deleteTeam(this.state._id);
                  }}
                >
                  <DeleteOutlined
                    className="navBarText"
                    alt="Edit"
                    title="Edit"
                    style={{ fontSize: "20px" }}
                  />
                </Popconfirm>
              ) : null}
            </Col>
            <Col span={20}></Col>
          </Row>
        </Drawer>
      </div>
    );
  }
}
