// import firebase from "firebase/app";
// import "firebase/storage";
import firebase from "firebase/compat/app";
import "firebase/compat/storage";

const firebaseConfig = {
  apiKey: "AIzaSyAnvJqGDil8_DXP3yM_044g2FNv8QmUjZo",
  authDomain: "testcode-1814c.firebaseapp.com",
  projectId: "testcode-1814c",
  storageBucket: "testcode-1814c.appspot.com",
  messagingSenderId: "932564849357",
  appId: "1:932564849357:web:de28f4944182b3bea1cf84",
  measurementId: "G-JKK64YMLSC",
};

firebase.initializeApp(firebaseConfig);
const storage = firebase.storage();

export { storage, firebase as default };
