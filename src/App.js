import "./App.css";
import { Link, Routes, BrowserRouter, Route } from "react-router-dom";
import React, { Component } from "react";

import Main from "./components/Main";
import "antd/dist/antd.css";
export class App extends Component {
  render() {
    return (
      <>
        <BrowserRouter>
          {/* <NavbarComponent /> */}
          <Routes>
            <Route path="/" element={<Main />} />
          </Routes>
        </BrowserRouter>
      </>
    );
  }
}

export default App;
